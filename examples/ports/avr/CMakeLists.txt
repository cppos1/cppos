project(c++os-example-helpers)

set(C++OS_EXAMPLE_HELPER_SOURCES
  ex-helpers.cc
  outStream.cc
)

add_library(c++OsExHelpersLib ${C++OS_EXAMPLE_HELPER_SOURCES})
