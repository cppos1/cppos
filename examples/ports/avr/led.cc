// Task to blink an LED


#define _SFR_ASM_COMPAT 1
#define __SFR_OFFSET 0x20
#include "gpio.hh"

#include <chrono>
#include <thread>

#include <stdint.h>
#include <avr/io.h>


using namespace std::literals;

namespace
{
constexpr auto ledPort = PORTB;
constexpr uint8_t ledPin = 7;

constexpr auto ledFlashRate = 800ms;

GpioOut<ledPort, ledPin> led(true);
} // unnamed namespace

void ledFlashTask()
{

    while (1)
    {
        // we accept a slight slew due to processing time
        std::this_thread::sleep_for(ledFlashRate/2);
        led.toggle();
    }
}
