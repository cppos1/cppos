#ifndef AVR_EX_HELPERS_HH_INCLUDES
#define AVR_EX_HELPERS_HH_INCLUDES

#include <avr/interrupt.h>

inline void system_setup()
{
}

inline void no_sched_setup()
{
    sei();
}
#endif /* AVR_EX_HELPERS_HH_INCLUDES */
