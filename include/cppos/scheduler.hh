#ifndef _SCHEDULER_HH_INCLUDED
#define _SCHEDULER_HH_INCLUDED
#include "systraits.hh"
#include "helpers.hh"

#include <cstdint>
#include <array>
#include <bitset>

// for calls from assembler
extern "C"
{
void schedule_from_tick();
[[noreturn]] void error_handler();

extern void volatile * volatile * volatile previous_task_sp;
extern void volatile * volatile * volatile current_task_sp;
} // extern "C"

namespace std
{
class mutex;
class condition_variable;
} // namespace std

namespace cppos
{
namespace impl
{
constexpr int task_count =
    sys_traits::static_task_count + sys_traits::dynamic_task_count + 1;
class sched_helper;
} // namespace impl

namespace port
{
void yield();
} // namespace port

enum class task_state : uint8_t
{
    unused,
    blocked,
    ready,
    running,
};

struct task_block;

class event
{
public:
    event();

    //virtual bool check_wakeup(task_block *) = 0;

    template <std::invocable<task_block &> _F>
    void for_all_blocked_tasks_check_unblock(_F foo);

    void block(size_t idx)
    {
        blocked[idx] = true;
        unblocked[id_] = false;
    }

    void block();

    // return true if switch is required
    bool unblock_all();
    bool unblock_one();

    uint8_t id()
    {
        return id_;
    }

protected:
    std::bitset<impl::task_count> blocked;
    uint8_t id_;

    static std::bitset<sys_traits::max_event_count> unblocked;
    static uint8_t curEventCnt;

private:
    friend std::bitset<sys_traits::max_event_count>
    select(std::bitset<sys_traits::max_event_count>);
};

struct task_block
{
    void volatile *stack;
    void volatile *stack_end;
    // we probably don't need this, but for now we keep it
    event volatile *blocked_on = nullptr;
    uint32_t volatile timeout = 0;

    /**
     * we never use init_foo directly, but we need a place to store it
     * it may be a lambda with capture
     */
    nullary_function init_foo;

    size_t max_stack_size = 0;

    task_state volatile state = task_state::ready;
    uint8_t priority = sys_traits::default_priority;
    uint8_t id; // can be used by user for identification
    uint8_t idx = 255; // index in array
    uint8_t last_run = 0; // sub prio 
};

std::bitset<sys_traits::max_event_count>
select(std::bitset<sys_traits::max_event_count>);

class exec_context
{
public:
    exec_context();
    exec_context(exec_context &&) = delete;

    template <std::invocable _F>
    void add_static(_F &&foo,
                    uint8_t id,
                    int stack_size = sys_traits::default_stack_size,
                    uint8_t prio = sys_traits::default_priority);

    [[noreturn]] void start();

private:
    friend class event;
    friend class impl::sched_helper;
    friend class std::mutex;
    friend class std::condition_variable;
    friend void ::schedule_from_tick();
    friend void cppos::port::yield();
    friend void volatile *debug_handle();
    friend std::bitset<sys_traits::max_event_count>
    select(std::bitset<sys_traits::max_event_count>);

    void schedule();
    void add(nullary_function &&f, uint8_t id, int stack_size, uint8_t prio);

    std::array<task_block, impl::task_count> tasks;
    task_block *current_task = nullptr;
    std::byte *assigned_stack_top;

    uint8_t next_idx = 0;
    uint8_t current_run = 0;

    //static void yield();

    static exec_context *sys_sched;
};

template <uint8_t TaskId,
          unsigned StackSize = sys_traits::default_stack_size,
          uint8_t Priority = sys_traits::default_priority>
class scheduler
{
public:
    scheduler(exec_context *ctx)
      : context(ctx)
    {}

    template <std::invocable Foo>
    void execute(Foo &&f)
    {
        context->add_static(std::forward<Foo>(f),
                            TaskId, StackSize, Priority);
    }

private:
    exec_context *context;
};
} // namespace cppos
#endif /* _SCHEDULER_HH_INCLUDED */
