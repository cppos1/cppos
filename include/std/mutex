//  -*- C++ -*-
#ifndef _CPPOS_MUTEX_INCLUDED
#define _CPPOS_MUTEX_INCLUDED

// for now we do all with disabled interrupts
// so we don't even need atomics
//#include <atomic>
#include <cppos/scheduler.hh>
#include <port-header.hh>

namespace std
{
class mutex : public cppos::event
{
public:
    mutex() = default;
    mutex(mutex const &) = delete;
    mutex &operator=(mutex const &) = delete;

    void lock()
    {
        cppos::port::disable_interrupts();
        while (flag)
        {
            cppos::task_block *me = cppos::exec_context::sys_sched->current_task;
            block(me->idx);
            me->blocked_on = this;
            me->state = cppos::task_state::blocked;
            cppos::port::yield();
            // after return from yield() interrupts are enabled
            cppos::port::disable_interrupts();
        }

        flag = 1;
        cppos::port::enable_interrupts();
    }

    bool try_lock()
    {
        // as flag isn't atomic, we need interrupts disabled here as well
        cppos::port::interrupt_lock l;
        if (flag)
        {
            return false;
        }

        flag = 1;
        return true;
    }

    void unlock()
    {
        // this may be called with interrupts disabled
        bool irqs_enabled = cppos::port::interrupts_enabled();
        cppos::port::interrupt_lock l;
        flag = 0;
        if (blocked.any())
        {
            if (unblock_all() && irqs_enabled)
            {
                cppos::port::yield();
            }
        }
    }

private:
    uint8_t flag = 0;
};
} // namespace std

#include_next <mutex>

#endif /* _CPPOS_MUTEX_INCLUDED */
