
# the chip
set(MCU atmega2560)

# passed to the compiler as F_CPU
set(CLOCK 16000000L)

# the root toolchain directory
set(CMAKE_FIND_ROOT_PATH /opt/avr)

# the port to use
# for now this needs to be a subdirectory of cppos/src/ports
set(C++OS_PORT avr6)
# for now this needs to be a subdirectory of cppos/include/cppos/ports
set(C++OS_PORTHEADER avr6)

# directory for port of helpers for examples
# for now this needs to be a subdirectory of cppos/examples/ports
set(C++OS_EXAMPLES_PORT avr)

# if you also want include files to be found in you standard system,
# set these to BOTH or NEVER
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# TODO: hardcoded make
#set(CMAKE_MAKE_PROGRAM /usr/bin/make)

# this can be used to specify a specific compiler version ('-12' or '-git')
unset(COMPILER_SUFFIX)

# the default tools prefix, change if you have something else
set(CROSS_PREFIX avr-)

include(${CMAKE_CURRENT_LIST_DIR}/avr-toolchain.cmake)
